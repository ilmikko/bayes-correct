#!env ruby

require('io/console');
require('json');

$log=[];
def log(str)
	$log.push(str);
end
at_exit{
	File.write("./last.#{Time.now.to_i}.log",$log.join("\n"));
}

$keys=JSON.parse(File.read('./keys.json'));

def keydn(bytes,type)
	bytes[44]=type; # Keydown event
	return bytes.map{|x|x.chr}.join;
end

def getkey(key)
	bytes=[114,121,124,90,0,0,0,0,177,73,0,0,0,0,0,0,4,0,4,0,46,0,0,0,114,121,124,90,0,0,0,0,177,73,0,0,0,0,0,0,1,0,46,0,1,0,0,0,114,121,124,90,0,0,0,0,177,73,0,0,0,0,0,0,0,0,0,0,0,0,0,0]; # 72 bytes of goodness
	ind=$keys.index(key);
	return if ind.nil?;
	bytes[42]=ind+1;
	return bytes;
end

def put(key)
	if key[0]=="+"
		if key[1]=="+"
			# up
			bytes=getkey(key[2..-1]);
			return keydn(bytes,0);
		else
			# down
			bytes=getkey(key[1..-1]);
			return keydn(bytes,1);
		end
	else
		bytes=getkey(key);
		return keydn(bytes,1)+keydn(bytes,0);
	end
end

$convert={
	' ':"SPACE",
	'!':["+SHIFT","1","++SHIFT"],
	"\n":"ENTER"
};

def linuxstr(string)
	str='';

	string.split(//).map{|x|
		if $convert.key? x.to_sym
			$convert[x.to_sym]
		else
			x
		end
	}.flatten.each{|key|
		str+=put(key.upcase);
	};

	return str;
end

print(linuxstr("Hello from Ruby!"));
